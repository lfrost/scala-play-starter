/*
 * Copyright © 2017-2022 Lyle Frost <lfrost@cnz.com>
 */

import org.irundaia.sbt.sass.ForceScss

ThisBuild / organization := "com.cnz"
ThisBuild / scalaVersion := "2.13.8"
ThisBuild / version      := "1.2.0"

maintainer := "lfrost@cnz.com"

val gitBaseUrl = settingKey[String]("Git base URL")
gitBaseUrl := "https://gitlab.com/lfrost"

lazy val root = (project in file(".")).
    enablePlugins(PlayScala, SbtWeb).
    settings(
        name := "scala-play-starter",
        libraryDependencies ++= Seq(
            filters,
            guice,
            ws,
            "com.typesafe.play"      %% "play-json"          % "2.9.2",
            "org.scalatestplus.play" %% "scalatestplus-play" % "5.1.0" % "test"
        ),
        // https://docs.scala-lang.org/overviews/compiler-options/
        scalacOptions ++= Seq(
            // Standard Settings
            "--deprecation",            // Emit warning and location for usages of deprecated APIs.
            "--encoding", "UTF-8",      // Specify character encoding used by source files.
            "--explain-types",          // Explain type errors in more detail.
            "--feature",                // Emit warning and location for usages of features that should be imported explicitly.
            "--target:11",              // Target platform for object files. ([8],9,10,11,12)
            "--unchecked",              // Enable additional warnings where generated code depends on assumptions.

            // Advanced Settings

            // Warning Settings
            "-Ywarn-dead-code",         // Warn when dead code is identified.
            "-Ywarn-value-discard",     // Warn when non-Unit expression results are unused.
            "-Ywarn-numeric-widen",     // Warn when numerics are widened.
            "-Ywarn-unused:_",          // Enable or disable specific unused warnings: _ for all, -Ywarn-unused:help to list choices.
            "-Ywarn-extra-implicit",    // Warn when more than one implicit parameter section is defined.
            "-Xlint:_"                  // Enable or disable specific warnings: _ for all, -Xlint:help to list choices.
        ),
        Compile / doc / scalacOptions ++=
            Opts.doc.title(name.value)                                                                                             ++
            Opts.doc.sourceUrl(gitBaseUrl.value + "/" + name.value + "/tree/" + git.gitCurrentBranch.value + "€{FILE_PATH}.scala") ++
            Seq("-sourcepath", baseDirectory.value.getAbsolutePath),
        fork := true,
        // Enable and configure sbt-digest.
        pipelineStages := Seq(digest),
        DigestKeys.algorithms += "sha1",
        // Configure Sass (https://github.com/irundaia/sbt-sassify#options).
        SassKeys.syntaxDetection := ForceScss,
        // Exclude generated files from coverage report.
        coverageExcludedPackages := ";controllers\\.Reverse.*;controllers\\.javascript\\.Reverse.*;router\\.Routes.*"
    )
