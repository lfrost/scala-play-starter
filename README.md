# scala-play-starter

This is a starter template for creating Scala applications using Play Framework.  I also use it as a teaching tool.  It is designed to demonstrate several basic Play concepts in as concise and clear a manner as possible.  These concepts include the following.

* The basic view and controller structure, showing how routes are defined in conf/routes and wired to methods in controllers in app/controllers/, which then render views defined in app/views/.
* How to pass values to a view and how to do basic dynamic HTML in a view.
* How to define asset routes and access assets in views using the new Play 2.6 AssetsFinder.
* The difference between compiled assets in app/assets and static assets in public/.
* Scala compiler options for code linting.
* Using Scalastyle for code linting and style checking.
* Using sbt-web plugins, specifically the sbt-sassify plugin, to compile asset files located under app/assets/.
* Using ScalaTest for testing controllers.
* How to deploy a Play application with Docker.
* How to build a Play application in Jenkins.


## System Setup for Play Development

There are only two things needed for a local Scala / Play development environment: a Java 8+ SDK (either OpenJDK or Oracle Java will work) and sbt.  sbt is a build tool like Gradle and Maven.  It is available at https://www.scala-sbt.org/download.html.

The [official recommendation](https://docs.scala-lang.org/overviews/jdk-compatibility/overview.html) for Scala 2.13 is to use Java 8 or 11 for compiling Scala code.


## Running scala-play-starter

Here are the steps to build and run scala-play-starter.

1. Clone this repository.
1. cd into the base directory of the cloned repository.
1. If you are running this on a server instead of your local system, then run `export APP_HOST=domain-name`, where `domain-name` is the domain name of the server.
1. Run `sbt`.
1. In the sbt shell, run `clean`, then `compile`, then `scalastyle`, then `test`, and then `run`.  If you have previously generated a coverage report, also run `coverageOff` and `clean` first.
1. Access the running application at http://localhost:9000 and http://localhost:9000/this/is/a/path/.  (If you set `APP_HOST` above, then replace `localhost` with the value of `APP_HOST`.)
1. View the page source and note how the asset URIs have been generated in the HTML.
1. Press enter to stop the running application.
1. To generate a code coverage report, run `clean`, then `coverageOn`, then `test`, and then `coverageReport`.  The last command will show the filesystem path for the HTML coverage report.
1. Exit sbt with `exit`.

When you run `compile`, there will be several warnings from the Play-generated Scala files.  With a little experience, it is not difficult to distinguish between warnings from Play-generated files and your own code.


## Deploying scala-play-starter With Docker.

You will need to have Docker running to do this.  See https://docs.docker.com/engine/installation/.

For security, we will not store the HTTP secret in the Docker image.  For flexibility, we will also specify the domain name at run time rather than build time.

Use the following to generate an application secret and store it in a file that will not be part of the Docker image.

```bash
# Generate a secret and save it to a Docker environment file.
sbt playGenerateSecret | grep ' secret: ' | sed -e 's/^.* secret: /HTTP_SECRET=/' > .docker-env
# If this will be accessed at something other than localhost, then add the host name to the Docker environment file.
echo 'APP_HOST=domain-name' >> .docker-env
# Because the Docker environment file contains a secret, make it accessible only by the file owner.
chmod 0600 .docker-env
```

Use the following command to create a Docker image for scala-play-starter.  There are currently Dockerfiles for Debian and Fedora.

```bash
sbt clean dist
docker build --no-cache                                         \
    --build-arg BUILD_DATE="$(date -u +'%Y-%m-%dT%H:%M:%SZ')"   \
    --build-arg VCS_REF="$(git rev-parse HEAD)"                 \
    --file Dockerfile                                           \
    --tag scala-play-starter .
```

Documentation for the docker build command can be found at https://docs.docker.com/engine/reference/commandline/build/.

Run the scala-play-starter image in a Docker container.

```bash
docker run                      \
    --rm                        \
    --env-file .docker-env      \
    -p9000:9000                 \
    scala-play-starter
```

To view access a shell on the container (in order to view log files, etc.), first use `docker ps` to obtain the container ID, then run `docker exec -it container-id /bin/bash` using that container ID.

Documentation for the docker run command can be found at https://docs.docker.com/engine/reference/commandline/run/.

Access the Docker container at http://localhost:9000.  (If you set `APP_HOST` above, then replace `localhost` with the value of `APP_HOST`.)


## Scala and Play Documentation

* [sbt documentation](https://www.scala-sbt.org/learn.html)
* [Scala Standard Library API](https://www.scala-lang.org/api/2.13.8/)
* [Scala style guidelines](https://docs.scala-lang.org/style/)
* [Scalastyle rules](https://scalastyle.beautiful-scala.com/rules-1.5.0.html)
* [Play manual](https://www.playframework.com/documentation/2.8.x/Home)
* [Play API](https://www.playframework.com/documentation/2.8.x/api/scala/index.html)
* [sbt-web documentation and list of plugins](https://github.com/sbt/sbt-web)
